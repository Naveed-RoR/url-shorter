# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
2.5.8 (currently using this for current project

* Rails version
5.2.6 (current project version)

* System dependencies
Git, Ruby, Rails, PostgreSQL should be installed before you test this

* Configuration
Provide/Edit database.yml

* Database creation
bundle exec rake db:create

* Database initialization
bundle exec rake db:migrate

* How to run the test suite
bundle exec rails test
