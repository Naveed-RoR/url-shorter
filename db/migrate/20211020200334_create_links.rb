class CreateLinks < ActiveRecord::Migration[5.2]
  def change
    create_table :links do |t|
      t.text :url
      t.string :slug
      t.integer :clicks, default: 0
      t.references :user, foreign_key: true

      t.timestamps null: false
    end
  end
end
