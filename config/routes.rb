Rails.application.routes.draw do
  root to: "pages#home"
  devise_for :users
  resources :links, only: [:index, :create]

  get '/s/:slug', to: 'links#show', as: :short
end
