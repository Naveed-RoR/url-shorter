require 'test_helper'

class LinkTest < ActiveSupport::TestCase

  test 'valid link' do 
    link = Link.new(url: "https://abc.xyz")
    assert link.valid?
  end

  test 'invalid without url' do
    link = Link.new
    refute link.valid?
  end

  test 'invalid if url is smaller than 7 characters' do
    link = Link.new(url: "http:")
    refute link.valid?
  end

  test 'invalid if url is greater than 7 characters' do
    link = Link.new(url: "http://" + SecureRandom.urlsafe_base64(50000).to_s)
    refute link.valid?
  end

  test 'invalid if slug is not unique' do
    link = Link.create(url: "https://abc.xyz")
    link1 = Link.new(url: "https://abc.qwe", slug: link.slug)
    refute link1.valid?
  end

  test '#user' do
    link = links(:one)
    assert_equal users(:one), link.user
  end

  test '#short' do
    link = links(:one)
    assert_match /#{link.slug}/, link.short
  end

  test '#generate_slug' do
    link = Link.new(url: "https://abc.xyz")
    refute link.slug
    assert link.valid?
    assert_equal 6, link.slug.length
  end
end
