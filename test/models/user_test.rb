require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test '#links' do
    user = users(:one)
    assert_equal 1, user.links.length
  end
end
