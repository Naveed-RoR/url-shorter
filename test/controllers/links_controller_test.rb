require 'test_helper'

class LinksControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should create Link' do 
    assert_difference("Link.count") do 
      post links_url, params: { link: { url: "http://abc.xyz" } }
    end

    assert_redirected_to root_path
  end

  test 'should increase clicks count on show' do
    link          = links(:one)
    previous_count = link.clicks

    assert_difference("link.reload.clicks") do
      get short_path(slug: link.slug)
    end

    assert_redirected_to link.url
  end

  test 'should get index' do
    user = users(:one)
    sign_in user
    get links_path
    assert_match "Saved Links", @response.body
    assert_match user.links.first.short, @response.body
  end
end
