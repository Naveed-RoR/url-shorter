class LinksController < ApplicationController
  # hooks
  before_action :authenticate_user!, only: [:index]

  def show
    @link = Link.find_by_slug(params[:slug]) 
    return redirect_to root_path, alert: "Link not found!" if @link.nil?
    @link.update_attribute(:clicks, @link.clicks + 1)
    redirect_to @link.url
  end

  def create
    links = current_user.present? ? current_user.links : Link
    @link = links.find_or_create_by(link_params)
    if @link.valid?
      redirect_to root_path, notice: @link.short
    else
      redirect_to root_path, alert: @link.errors.full_messages.join(", ")
    end
  end

  def index
    @links = current_user.links
  end

  private
    def link_params
      params.require(:link).permit(:url)
    end
end
