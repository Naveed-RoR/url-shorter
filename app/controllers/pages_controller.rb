class PagesController < ApplicationController
  def home
    @link = current_user.present? ? current_user.links.build : Link.new
  end
end
