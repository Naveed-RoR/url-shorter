class Link < ApplicationRecord
  # validations
  validates :url, presence: true, format: URI::regexp(%w[http https]), length: { within: 7..65536, on: :create, messages: "too long"}
  validates :slug, presence: true, uniqueness: true

  # associations
  belongs_to :user, optional: true

  # hooks
  before_validation :generate_slug

  # returns short url
  def short
    Rails.application.routes.url_helpers.short_url(slug: self.slug)
  end

  private
    def generate_slug
      self.slug = SecureRandom.urlsafe_base64[0..5] if self.slug.nil? || self.slug.empty?
      true
    end
end
